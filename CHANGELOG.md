# Changelog

## 20201101

### Added

- Player movement
- dev world

## 20201102

### Added

- Signs
- Place 
- indicator

## 20201103

### Added

- Walk state machine
- Minimap

## 20201104

### Changed 

- Set usebutton to E

## 20201108

### Removed

- Minimap
- Old textures

### Added

- New grass and path textures
- Sample for player texture
- Added new test world


## 20201111

### Added

- Simple enemy AI