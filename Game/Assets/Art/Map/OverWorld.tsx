<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="OverWorld" tilewidth="16" tileheight="16" tilecount="1440" columns="40">
 <image source="../Texture Pack/OverWorld.png" width="640" height="576"/>
 <terraintypes>
  <terrain name="Dirt Path" tile="-1"/>
  <terrain name="Water Grass" tile="486"/>
 </terraintypes>
 <tile id="40" terrain=",,,0"/>
 <tile id="41" terrain=",,0,0"/>
 <tile id="42" terrain=",,0,"/>
 <tile id="80" terrain=",0,,0"/>
 <tile id="81" terrain="0,0,0,0"/>
 <tile id="82" terrain="0,,0,"/>
 <tile id="120" terrain=",0,,"/>
 <tile id="121" terrain="0,0,,"/>
 <tile id="122" terrain="0,,,"/>
 <tile id="240" terrain="0,0,0,"/>
 <tile id="241" terrain="0,0,,0"/>
 <tile id="280" terrain="0,,0,0"/>
 <tile id="281" terrain=",0,0,0"/>
 <tile id="360" terrain=",,,1"/>
 <tile id="361" terrain=",,1,1"/>
 <tile id="362" terrain=",,1,"/>
 <tile id="400" terrain=",1,,1"/>
 <tile id="401" terrain="1,1,1,1"/>
 <tile id="402" terrain="1,,1,"/>
 <tile id="440" terrain=",1,,"/>
 <tile id="441" terrain="1,1,,"/>
 <tile id="442" terrain="1,,,"/>
 <tile id="480" terrain="1,1,1,"/>
 <tile id="481" terrain="1,1,,1"/>
 <tile id="520" terrain="1,,1,1"/>
 <tile id="521" terrain=",1,1,1"/>
</tileset>
