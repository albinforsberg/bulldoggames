﻿using System.Diagnostics;
using System.Security.Cryptography;
using UnityEngine;

public enum PlayerState{
    walk,
    attack,
    inteact
}

public class PlayerMovement : MonoBehaviour
{
    public PlayerState currentState;
    public float speed;
    private Rigidbody2D MyRigidBody;
    private Vector3 change;
    // private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        currentState = PlayerState.walk; // Start State
        // animator = GetComponent<Animator>();
        MyRigidBody = GetComponent<Rigidbody2D>();
        // animator.SetFloat("MoveX", 0);
        // animator.SetFloat("MoveY", -1);
    }

    // Update is called once per frame
    void Update()
    {
        // Character movements
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
        updateAnimationAndMove();

    }

    void updateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharacter();
            // animator.SetFloat("MoveX", change.x);
            // animator.SetFloat("MoveY", change.y);
            // animator.SetBool("Moving", true);
        }
        else
        {
            // animator.SetBool("Moving", false);
        }
    }

    void MoveCharacter()
    {
        change.Normalize();
        MyRigidBody.MovePosition(
             transform.position + change * speed * Time.deltaTime
            );
    }
}
