# Bulldoggames

## Contents
[[_TOC_]]

## About

### Tools used

- Unity
    - Version 2020.1.11f1
- [Tiled](https://www.mapeditor.org/)
- [SuperTiled2Unity](https://seanba.itch.io/supertiled2unity)
- [Git](https://git-scm.com/)

## Update Unity Licence

- Click on this [link](https://id.unity.com/en/serials) and sign in with your Unity account.
- Remove previous licence.
- Go to Unity Hub and activate your licence with the serial key displayed on the page.

## Git commands

### To clone (first time)

```bash
git clone https://gitlab.com/albin02forsberg/bulldoggames.git
```

### To update all files

Do this befor you start working on the game.

```bash
git pull
```

### Create a new/change branch
Do this for every feature your working on. 

```bash
git branch name
git checkout name
```

**Example**

```bash
git branch LevelDesign
git checkout LevelDesign
```

### Check files changed

```bash
git status
```

### Upload to GitLab

Be in the root directory of the github folder

```bash
git add .
git commit -m "A descriptive message (short)"
git push
```